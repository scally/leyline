import React, {useRef} from 'react'
import {View, StyleSheet, StatusBar} from 'react-native'
import {ECSArena} from './ECSArena'

const useSimpleMovementSystem = () => {
  return ({eachEntity}) => {
    eachEntity({
      filter: ({entity}) => entity.position,
      callback: ({entity}) => {
        entity.position.x += 0.1
      }
    })
  }
}

const Player = ({position, size}) => {
  return <View style={
    {
      position: 'absolute',
      left: position.x,
      top: position.y,
      width: size.width,
      height: size.height,
    }
  } />
}

const App = () => {
  const initialEntities = useRef({
    player: {
      position: {x: 40, y: 200},
      size: {width: 32, height: 32},
      renderer: Player,
    },
  })

  const systems = useRef([
    useSimpleMovementSystem()
  ])

  return (
    <>
      <StatusBar hidden={true} />
      <View style={styles.container}>
        <ECSArena
          initialEntities={initialEntities.current}
          systems={systems.current}
        />
      </View>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'cyan',
  },
})

export default App
