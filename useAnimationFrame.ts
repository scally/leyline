import {useRef, useCallback, useEffect} from 'react'

export interface UseAnimationFrameOptions {
  shouldStartOnLoad: boolean
}

export const useAnimationFrame = (
  onFrame,
  options: UseAnimationFrameOptions = {shouldStartOnLoad: true},
) => {
  const {shouldStartOnLoad} = options
  const loopId = useRef(null)

  const start = useCallback(() => {
    if (!loopId.current) {
      loop(null)
    }
  }, [])

  const stop = useCallback(() => {
    if (loopId.current) {
      cancelAnimationFrame(loopId.current)
      loopId.current = null
    }
  }, [])

  const loop = useCallback(time => {
    if (loopId.current) {
      onFrame(time)
    }

    loopId.current = requestAnimationFrame(loop)
  }, [])

  useEffect(() => {
    if (shouldStartOnLoad) {
      start()
    }

    return () => {
      stop()
    }
  }, [])

  return [start, stop]
}
