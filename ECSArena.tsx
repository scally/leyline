import React, {useEffect} from 'react'
import {View} from 'react-native'
import {useECS} from './useECS'

export const ECSArena = ({initialEntities, systems}) => {
  const {entities, onTouchStart, start} = useECS(initialEntities, systems)

  useEffect(() => {
    start()
  }, [])

  return (
    <View style={{flex: 1}} onTouchStart={onTouchStart}>
      {Object.keys(entities)
        .filter(entityKey => entities[entityKey].renderer)
        .map(entityKey => {
          const entity = entities[entityKey]
          return <entity.renderer key={entityKey} {...entity} />
        })}
    </View>
  )
}
