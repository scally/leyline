import {useEffect, useRef, useCallback} from 'react'
import {useImmer} from 'use-immer'
import {useAnimationFrame} from './useAnimationFrame'

export const forEachEntity = (entities: {}) => ({filter, callback}) => {
  for (const entityKey in entities) {
    const entity = entities[entityKey]
    if (!filter({entity, entityKey})) continue

    callback({entity, entityKey})
  }
}

export const useECS = (initialEntities: {}, systems: any[]) => {
  const [entities, updateEntities] = useImmer(initialEntities)
  const touches = useRef([])

  useEffect(() => {
    touches.current = []
  })

  const previousTime = useRef(null)
  const [start, stop] = useAnimationFrame(
    time => {
      const delta = time - (previousTime.current || time)
      systems.forEach(system =>
        system({
          entities,
          updateEntities,
          eachEntity: ({filter, callback}) => {
            updateEntities(draft => {
              forEachEntity(draft)({filter, callback})
            })
          },
          touches: touches.current,
          time: {
            currentTime: time,
            delta,
          },
        }),
      )
    },
    {shouldStartOnLoad: false},
  )

  const onTouchStart = useCallback(() => {
    touches.current = ['up']
  }, [])

  return {
    entities,
    onTouchStart,
    start,
    stop,
  }
}
